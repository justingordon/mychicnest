<html>
    <head>
        <title>Untitled Document</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #FAFAFA;width: 100%;">
     <center>
         <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="margin: 0;padding: 0;background-color: #FAFAFA;height: 100%;width: 100%;">
             <tr>
                 <td align="center" valign="top" style="border-collapse: collapse;">
                        <!-- // Begin Template Preheader \ -->
                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader" style="background-color: #FAFAFA;">
                            <tr>
                                <td valign="top" class="preheaderContent" style="border-collapse: collapse;">
                                
                                 <!-- // Begin Module: Standard Preheader  -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                     <tr>
                                         <td valign="top" style="border-collapse: collapse;">
                                             <div style="color: #505050;font-family: Arial;font-size: 10px;line-height: 100%;text-align: left;">Your order (Order#: $(OrderNo)) is scheduled to ship per the following tracking information:</div>
                                            </td>
                                        </tr>
                                    </table>
                                 <!-- // End Module: Standard Preheader  -->
                                
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Preheader \ -->
                        
                     <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer" style="border: 1px solid #DDDDDD;background-color: #FFFFFF;">
                         <tr>
                             <td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Header \ -->
                                 <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader" style="background-color: #FFFFFF;border-bottom: 0;">
                                        <tr>
                                            <td class="headerContent" style="border-collapse: collapse;color: #202020;font-family: Arial;font-size: 34px;font-weight: bold;line-height: 100%;padding: 0;text-align: center;vertical-align: middle;">
                                            
                                             <!-- // Begin Module: Standard Header Image \ -->
                                             <a href="$(HomeURL)" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;"><img src="http://www.mychicnest.com/v/vspfiles/email_templates/images/mychicnest_email_logo.gif" alt="MyChicNest.com" style="max-width: 600px;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" id="headerImage campaign-icon"></a>
                                             <!-- // End Module: Standard Header Image \ -->
                                            
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td valign="top" width="100%" style="border-collapse: collapse;">
                                             <table border="0" cellpadding="20" cellspacing="0" width="100%" style="border-top:1px solid #cccccc;">
                                                 <tr>
                                                     <td valign="top" class="bodyContent" style="border-collapse: collapse;background-color: #FFFFFF;">
                                                     <div style="color: #505050;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;">Hello $(FirstName),
                                                     <br>
                                                     Your order (Order#: $(OrderNo)) is scheduled to ship per the following tracking information:<br>
                                                     <br> $(TrackingNumbers)<br>
                                                     </div>
                                            		 </td>
                                                  </tr>
                                              </table>
                                            </td>
                                         </tr>
                                    </table>
                                    <!-- // End Template Header \ -->
                                </td>
                            </tr>
                         <tr>
                             <td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Body \ -->
                                 <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody" style="border-top:1px solid #cccccc;border-bottom:1px solid #cccccc;">
                                     <tr>
                                         <td valign="top" width="350" style="border-right: 1px solid #cccccc;border-collapse: collapse;">
                                             <table border="0" cellpadding="0" cellspacing="0" width="350">
                                                 <tr>
                                                     <td valign="top" style="border-collapse: collapse;">
                                             <table border="0" cellpadding="0" cellspacing="0" width="350">
                                                             <tr>
                                                                 <td valign="top" class="bodyContent" style="border-collapse: collapse;background-color: #FFFFFF;">
                                                            
                                                                        <!-- // Begin Module: Standard Content \ -->
                                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td valign="top" style="border-collapse: collapse;">
                                                                                <div style="color: #505050;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;">
                                                                                    <h1 class="h1" style="color: #202020;display: block;font-family: Arial;font-size: 16px;font-weight: bold;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 15px;margin-left: 0;text-align: left;">How To Track Your Shipment:</h1>
                                                                                    <h2 class="h2" style="color:#000000; display: block;font-family: Arial;font-size: 14px;font-weight: bold;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">FedEx: <a href="http://www.fedex.com/us/" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">www.fedex.com/us/</a></h2>
                                                                                    <ul style="padding-left:5px; color:#505050; font-family:Arial; font-size:14px; line-height:150%; text-align:left;">
                                                                                    <li style="margin-bottom:5px;">Enter your tracking number under &quot;Track Shipments&quot; in the middle of the page.</li>
                                                                                    <li>Please allow one business day for your tracking number to activate.</li>
                                                                                    </ul>
                                                                                    
                                                                                    
                                                                                    
                                                                                    <h2 class="h2" style="color:#000000; display: block;font-family: Arial;font-size: 14px;font-weight: bold;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">YRC Freight: <a href="http://yellowtracking.stidelivers.com/" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">http://yellowtracking.stidelivers.com</a></h2>
                                                                                    <ul style="padding-left:5px; color:#505050;font-family:Arial; font-size:14px; line-height:150%; text-align:left; margin-bottom:25px;">
                                                                                    <li style="margin-bottom:5px;">Enter your tracking # in the provided field and click on &quot;Track My Shipment.&quot;</li>
                                                                                    <li style="margin-bottom:5px;">Note: Tracking information will show &quot;delivered&quot; once your order arrives to your local delivery agent</li>
                                                                                    <li style="margin-bottom:5px;">Your local YRC delivery agent will contact you within 48 hours of receiving your order to schedule a mutually convenient delivery time.</li>
                                                                                    </ul>
                                                                                    
                                                                                    
                                                                                    <h2 class="h2" style="color:#000000; display: block;font-family: Arial;font-size: 14px;font-weight: bold;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Home Direct USA: <a href="http://www.homedirectusa.com" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">www.homedirectusa.com</a></h2>
                                                                                    <ul style="padding-left:5px; color:#505050;font-family:Arial; font-size:14px; line-height:150%; text-align:left; margin-bottom:25px;">
                                                                                    <li style="margin-bottom:5px;">Enter your waybill number in the appropriate field under &quot;Track Your Delivery&quot; on the right side of the page.</li>
                                                                                    <li>Your local Home Direct delivery agent will contact you in 7 to 10 business days to schedule your delivery.</li>
                                                                                    </ul>
                                                                                    
                                                                                    <h2 class="h2" style="color:#000000; display: block;font-family: Arial;font-size: 14px;font-weight: bold;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Central Freight: <a href="http://www.centralfreight.com/" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">www.centralfreight.com</a></h2>
                                                                                    <ul style="padding-left:5px; color:#505050;font-family:Arial; font-size:14px; line-height:150%; text-align:left;">
                                                                                    <li>Enter your PRO number under &quot;Quick Track&quot; on the left side of the page.</li>
                                                                                    </ul>
                                                                                    <h2 class="h2" style="color:#000000; display: block;font-family: Arial;font-size: 14px;font-weight: bold;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Estes Express: <a href="http://www.estes-express.com/" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">www.estes-express.com</a></h2>
                                                                                    <ul style="padding-left:5px; color:#505050;font-family:Arial; font-size:14px; line-height:150%; text-align:left;">
                                                                                    <li>Enter your PRO number under &quot;Shipment Tracking&quot; on the right side of the page.</li>
                                                                                    </ul>
                                                                                    <h2 class="h2" style="color:#000000; display: block;font-family: Arial;font-size: 14px;font-weight: bold;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Vitran: <br><a href="http://www.vitranexpress.com/Tracing/Tracing1.aspx" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">www.vitranexpress.com/Tracing/Tracing1.aspx</a></h2>
                                                                                    <ul style="padding-left:5px; color:#505050;font-family:Arial; font-size:14px; line-height:150%; text-align:left;">
                                                                                    <li>Enter your PRO number under &quot;Shipment Tracking&quot; on the center of the page.</li>
                                                                                    </ul>
                                                                                </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // End Module: Standard Content \ -->
                                                            
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                 </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <!-- // Begin Sidebar \ -->
                                         <td valign="top" width="250" id="templateSidebar" style="border-collapse: collapse;background-color: #FFFFFF;">
                                             <table border="0" cellpadding="0" cellspacing="0" width="250">
                                                 <tr>
                                                     <td valign="top" class="sidebarContent" style="border-collapse: collapse;">
                                                           
                                                            <!-- // Begin Module: Top Image with Content \ -->
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" style="border-collapse: collapse;">
                                                                        <div style="color: #505050;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;">
                                                                            <h1 class="h1" style="color: #202020;display: block;font-family: Arial;font-size: 16px;font-weight: bold;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 15px;margin-left: 0;text-align: left;">Damage Policy Reminders:</h1>
                                                                            <u>You must notate any noticeable shipping damage on your delivery receipt.</u> This includes damage to the packaging and to the package contents.
                                                                            <br>
                                                                            <br>
                                                                            If you do not notate &quot;DAMAGED IN TRANSIT&quot; on your delivery receipt, we cannot guarantee that your delivery will be repaired, replaced, or otherwise remedied.
                                                                            <br>
                                                                            <br>
                                                                            FedEx deliveries will not have an opportunity to mark a delivery receipt unless you have specifically requested Signature Required for your shipment.
                                                                            <br>
                                                                            <br>
                                                                            <h2 class="h2" style="color:#000000; display: block;font-family: Arial;font-size: 14px;font-weight: bold;line-height: 150%;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Damage claims must be submitted to us within 24 hours of delivery.</h2>
                                                                            <ul style="padding-left:5px;">
                                                                            <li>Submit a <a href="https://www.mychicnest.com/Articles.asp?ID=264" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">Claim Form</a></li>
                                                                            <li>Email pictures to <a href="mailto:customerservice@mychicnest.com" style="color: #336699;font-weight: normal;text-decoration: underline; font-size: 12px;">customerservice@mychicnest.com</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // End Module: Top Image with Content \ -->
                                                        
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <!-- // End Sidebar \ -->
                                        </tr>
                                    </table>
                                    <!-- // End Template Body \ -->
                                </td>
                            </tr>
                         <tr>
                             <td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Footer \ -->
                                 <table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter" style="background-color: #FFFFFF;border-top: 0;">
                                     <tr>
                                         <td valign="top" class="footerContent" style="border-collapse: collapse;">
                                            
                                                <!-- // Begin Module: Standard Footer \ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="utility" style="border-collapse: collapse;background-color: #FFFFFF;border: 0;">
                                                            <div style="color: #707070;font-family: Arial;font-size: 12px;line-height: 125%;text-align: center;">
                                                                <strong>Thanks again for shopping at $(StoreName)!</strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Standard Footer \ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \ -->
                                </td>
                            </tr>
                        </table>
                        <br>
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
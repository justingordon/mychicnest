/* Opens the drawer when the menu icon is clicked, or opens the mobile search window when the search icon is clicked. */

//  menu variables
var mobileMenu = document.querySelector('#mobile-menu-icon img');
var mobileNavSearch = document.querySelector('#mobile-nav');

//  search variables
var searchIcon = document.querySelector('#mobile-search-icon img');
var searchBox = document.querySelector('#mobile-search');

//  variable for clicking off nav
var main = document.querySelector('main#content_area');


//  Toggles the nav drawer and search box on nav button
mobileMenu.addEventListener('click', function(e) {
    mobileNavSearch.classList.toggle('open');
    searchBox.classList.toggle('open');
    e.stopPropagation();
});

main.addEventListener('click', function() {
    mobileNavSearch.classList.remove('open');
    searchBox.classList.remove('open');
});


//  Toggles the nav drawer and search box on search button
searchIcon.addEventListener('click', function(e) {
    searchBox.classList.toggle('open');
    e.stopPropagation();
});

main.addEventListener('click', function() {
    searchBox.classList.remove('open');
});




/* ----------------- ALL CODE BELOW IS FROM THE OLD MAIN.JS ------------------- */

// Array of all product codes for the default set of product options (material/finishes)
var allFabrics = new Array('28194Sofa', '28924', '30366Sectional', '30366Sofa', '503', '505', '506', '510', '511', '512', '513', '514', '515', '516', '518', '519', '520', '521', '522', '523', '524', '526', '527', '528', '529', '531', '532', '533', '534', '535', '536', '539', '540', '541', '543', '544', '548', '549', '551', '552', '553', '555', '556', '557', '558', '559', '560', '562', '564', '573', '574', '575', '576', '577', '593', '597', '609', '610', '660', '661', '662', '664', '665', '668', '678', '685', '686', '689', 'FT827', 'FT827ASectional', 'FT827B-Loveseat', 'FT827B-ThreeSeat', 'FT827B-TwoSeat', 'FT828Sectional', 'FT828Sofa', 'FT82A-Fourseat');

function searchFabrics(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table class="swatches"></table>').insertAfter('table.colors_pricebox:first-of-type');
        $('table.swatches').append('<tr><td><div class="btn-request-swatches"><a target="_blank" href="/Articles.asp?ID=279"><img alt="Request Swatches" src="v/vspfiles/templates/mcn_2015/images/buttons/btn-request-swatches.gif"></a></div><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Polyester</p><div id="polyester"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Poly Blends</p><div id="linen-poly-blends"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sPolyester();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenPolyBlends();

        swThumbEffect();

    }
}


// Default product options EXCEPT leatherette is missing as the manufacturer can only product it for some tufted products
var noLeatherettes = new Array('550', '561', '563');

function searchNoLeatherettes(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table class="swatches"></table>').insertAfter('table.colors_pricebox:first-of-type');
        $('table.swatches').append('<tr><td><div class="btn-request-swatches"><a target="_blank" href="/Articles.asp?ID=279"><img alt="Request Swatches" src="v/vspfiles/templates/mcn_2015/images/buttons/btn-request-swatches.gif"></a></div><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Polyester</p><div id="polyester"></div><p class="swatchTitle">Luxury Velvets</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Poly Blends</p><div id="linen-poly-blends"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sPolyester();
        sLuxuryVelvets();
        sVelvets();
        sLinenPolyBlends();

        swThumbEffect();

    }
}



// Default swatches with wood options and nailhead finishes
var swHarper = new Array('600', '601', '603', '604', '605');

function searchHarper(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table class="swatches"></table>').insertAfter('table.colors_pricebox:first-of-type');
        $('table.swatches').append('<tr><td><div class="btn-request-swatches"><a target="_blank" href="/Articles.asp?ID=279"><img alt="Request Swatches" src="v/vspfiles/templates/mcn_2015/images/buttons/btn-request-swatches.gif"></a></div><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Polyester</p><div id="polyester"></div><p class="swatchTitle">Luxury Velvets</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Poly Blends</p><div id="linen-poly-blends"></div><p class="swatchTitle">Wood Finishes</p><div id="wood-finishes"></div><p class="swatchTitle">Nailhead Finishes</p><div id="nailhead-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sPolyester();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenPolyBlends();
        sWoodFinishes();
        sNailheadFinishes();

        swThumbEffect();

    }
}




// Default swatches with wood options
var swDelilah = new Array('525', '537', '538', '565', '566', '567', '568', '570', '571', '572', '583', '584', '585', '606', '639', '680');

function searchDelilah(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table class="swatches"></table>').insertAfter('table.colors_pricebox:first-of-type');
        $('table.swatches').append('<tr><td><div class="btn-request-swatches"><a target="_blank" href="/Articles.asp?ID=279"><img alt="Request Swatches" src="v/vspfiles/templates/mcn_2015/images/buttons/btn-request-swatches.gif"></a></div><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Polyester</p><div id="polyester"></div><p class="swatchTitle">Leatherette</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Poly Blends</p><div id="linen-poly-blends"></div><p class="swatchTitle">Wood Finishes</p><div id="wood-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sPolyester();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenPolyBlends();
        sWoodFinishes();

        swThumbEffect();

    }
}


// Default swatches with Metal Finishes
var swMetalFinishes = new Array('500', '569');

function searchMetalFinishes(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table class="swatches"></table>').insertAfter('table.colors_pricebox:first-of-type');
        $('table.swatches').append('<tr><td><div class="btn-request-swatches"><a target="_blank" href="/Articles.asp?ID=279"><img alt="Request Swatches" src="v/vspfiles/templates/mcn_2015/images/buttons/btn-request-swatches.gif"></a></div><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Polyester</p><div id="polyester"></div><p class="swatchTitle">Leatherette</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Poly Blends</p><div id="linen-poly-blends"></div><p class="swatchTitle">Metal Finishes</p><div id="metal-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sPolyester();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenPolyBlends();
        sMetalFinishes();

        swThumbEffect();

    }
}


// Default swatches with Nailhead Finishes
var swNailheadFinishes = new Array('502', '504', '507', '508', '509', '517', '530', '542', '554', '607');

function searchNailheadFinishes(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table class="swatches"></table>').insertAfter('table.colors_pricebox:first-of-type');
        $('table.swatches').append('<tr><td><div class="btn-request-swatches"><a target="_blank" href="/Articles.asp?ID=279"><img alt="Request Swatches" src="v/vspfiles/templates/mcn_2015/images/buttons/btn-request-swatches.gif"></a></div><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Polyester</p><div id="polyester"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Poly Blends</p><div id="linen-poly-blends"></div><p class="swatchTitle">Nailhead Finishes</p><div id="nailhead-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sPolyester();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenPolyBlends();
        sNailheadFinishes();

        swThumbEffect();

    }
}

// Default swatches with both Metal and Nailhead Finishes
var swNailheadMetalFinishes = new Array('');

function searchNailheadMetalFinishes(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table class="swatches"></table>').insertAfter('table.colors_pricebox:first-of-type');
        $('table.swatches').append('<tr><td><div class="btn-request-swatches"><a target="_blank" href="/Articles.asp?ID=279"><img alt="Request Swatches" src="v/vspfiles/templates/mcn_2015/images/buttons/btn-request-swatches.gif"></a></div><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Polyester</p><div id="polyester"></div><p class="swatchTitle">Leatherette</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Poly Blends</p><div id="linen-poly-blends"></div><p class="swatchTitle">Metal Finishes</p><div id="metal-finishes"></div><p class="swatchTitle">Nailhead Finishes</p><div id="nailhead-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sPolyester();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenPolyBlends();
        sMetalFinishes();
        sNailheadFinishes();

        swThumbEffect();

    }
}



// Default swatches with wood and metal finishes
var swWoodMetalFinishes = new Array('');

function searchWoodMetalFinishes(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table class="swatches"></table>').insertAfter('table.colors_pricebox:first-of-type');
        $('table.swatches').append('<tr><td><div class="btn-request-swatches"><a target="_blank" href="/Articles.asp?ID=279"><img alt="Request Swatches" src="v/vspfiles/templates/mcn_2015/images/buttons/btn-request-swatches.gif"></a></div><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Polyester</p><div id="polyester"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Poly Blends</p><div id="linen-poly-blends"></div><p class="swatchTitle">Wood Finishes</p><div id="wood-finishes"></div><p class="swatchTitle">Metal Finishes</p><div id="metal-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenPolyBlends();
        sWoodFinishes();
        sMetalFinishes();

        swThumbEffect();

    }
}


/***********************************************************************************************
 Functions of product options (material/finish) to be called after adding the headings (see code above)
 ***********************************************************************************************/


/* Product options below are default options applied to all products ***************/

function sWovenPolyester(){
    $('#woven-polyester').append('<img src="v/vspfiles/swatches/woven-polyester/white-milford-ii.jpg" alt="White Milford II" /><img src="v/vspfiles/swatches/woven-polyester/metrolinen-latte.jpg" alt="Metrolinen Latte" /><img src="v/vspfiles/swatches/woven-polyester/almond-bennett-ii.jpg" alt="Almond Bennett II" /><img src="v/vspfiles/swatches/woven-polyester/chocolate-marlow-ii.jpg" alt="Chocolate Marlow II" /><img src="v/vspfiles/swatches/woven-polyester/blue-bird-marlow-ii.jpg" alt="Blue Bird Marlow II" /><img src="v/vspfiles/swatches/woven-polyester/dolphin-marlow-ii.jpg" alt="Dolphin Marlow II" /><img src="v/vspfiles/swatches/woven-polyester/charcoal-milford-ii.jpg" alt="Charcoal Milford II" /><img src="v/vspfiles/swatches/woven-polyester/metrolinen-grey.jpg" alt="Metrolinen Grey" /><div class="clear"></div>');
}

function sCottonPolyBlends(){$('#cotton-poly-blends').append('<img src="v/vspfiles/swatches/cotton-poly-blends/sherlock-grey.jpg" alt="Sherlock Grey" /><img src="v/vspfiles/swatches/cotton-poly-blends/sherlock-dark-grey.jpg" alt="Sherlock Dark Grey" /><img src="v/vspfiles/swatches/cotton-poly-blends/sherlock-chocolate.jpg" alt="Sherlock Chocolate" /><img src="v/vspfiles/swatches/cotton-poly-blends/savvy-quail.jpg" alt="Savvy Quail" /><img src="v/vspfiles/swatches/cotton-poly-blends/benan-purple.jpg" alt="Benan Purple" /><div class="clear"></div>');
}

function sPolyester(){$('#polyester').append('<img src="v/vspfiles/swatches/polyester/tuckerstone.jpg" alt="Tuckerstone" /><img src="v/vspfiles/swatches/polyester/sachi-glacier.jpg" alt="Sachi Glacier" /><img src="v/vspfiles/swatches/polyester/everglades021.jpg" alt="Everglades 021" /><img src="v/vspfiles/swatches/polyester/everglades-ever210.jpg" alt="Everglades Ever210" /><img src="v/vspfiles/swatches/polyester/dawson-carafe.jpg" alt="Dawson Carafe" /><img src="v/vspfiles/swatches/polyester/diamond-dove.jpg" alt="Diamond Dove" /><img src="v/vspfiles/swatches/polyester/diamond-espresso.jpg" alt="Diamond Espresso" /><img src="v/vspfiles/swatches/polyester/savona-surf.jpg" alt="Savona Surf" /><img src="v/vspfiles/swatches/polyester/savona-ink.jpg" alt="Savona Ink" /><img src="v/vspfiles/swatches/polyester/savona-fuchsia.jpg" alt="Savona Fuchsia" /><img src="v/vspfiles/swatches/polyester/sachi-ruby.jpg" alt="Sachi Ruby" /><img src="v/vspfiles/swatches/polyester/sachi-wheatgrass.jpg" alt="Sachi Wheatgrass" /><img src="v/vspfiles/swatches/polyester/sachi-black.jpg" alt="Sachi Black" /><div class="clear"></div>');
}

function sLeatherettes(){
    $('#leatherettes').append('<img src="v/vspfiles/swatches/leatherettes/lexi-white.jpg" alt="Lexi White" /><img src="v/vspfiles/swatches/leatherettes/ford-gold.jpg" alt="Ford Gold" /><img src="v/vspfiles/swatches/leatherettes/sizzle-bronze.jpg" alt="Sizzle Bronze" /><img src="v/vspfiles/swatches/leatherettes/ford-chestnut.jpg" alt="Ford Chestnut" /><div class="clear"></div>');
}

function sLuxuryVelvets(){
    $('#luxury-velvets').append('<img src="v/vspfiles/swatches/luxury-velvets/bella-pearl.jpg" alt="Bella Pearl" /><img src="v/vspfiles/swatches/luxury-velvets/emanuel-white.jpg" alt="Emanuel White" /><img src="v/vspfiles/swatches/luxury-velvets/bella-coffee.jpg" alt="Bella Coffee" /><img src="v/vspfiles/swatches/luxury-velvets/bella-granite.jpg" alt="Bella Granite" /><img src="v/vspfiles/swatches/luxury-velvets/liberty-charcoal.jpg" alt="Liberty Charcoal" /><img src="v/vspfiles/swatches/luxury-velvets/bella-pewter.jpg" alt="Bella Pewter" /><img src="v/vspfiles/swatches/luxury-velvets/zeros-cobalt-blue.jpg" alt="Zeros Cobalt Blue" /><img src="v/vspfiles/swatches/luxury-velvets/zeros-turquoise.jpg" alt="Zeros Turquoise" /><img src="v/vspfiles/swatches/luxury-velvets/bella-nile.jpg" alt="Bella Nile" /><img src="v/vspfiles/swatches/luxury-velvets/zeros-green.jpg" alt="Zeros Green" /><img src="v/vspfiles/swatches/luxury-velvets/milano-raspberry.jpg" alt="Milano Raspberry" /><img src="v/vspfiles/swatches/luxury-velvets/sera-purple.jpg" alt="Sera Purple" /><div class="clear"></div>');
}

function sVelvets(){
    $('#velvets').append('<img src="v/vspfiles/swatches/velvets/vanilla-1-benson.jpg" alt="Vanilla: 1 Benson" /><img src="v/vspfiles/swatches/velvets/mauve-33-benson.jpg" alt="Mauve: 33 Benson" /><img src="v/vspfiles/swatches/velvets/capri-36-benson.jpg" alt="Capri: 36 Benson" /><img src="v/vspfiles/swatches/velvets/shadow-empire-iii.jpg" alt="Shadow: Empire III" /><img src="v/vspfiles/swatches/velvets/dove-6-benson.jpg" alt="Dove: 6 Benson" /><img src="v/vspfiles/swatches/velvets/latte-25-benson.jpg" alt="Latte: 25 Benson" /><img src="v/vspfiles/swatches/velvets/truffle-18-benson.jpg" alt="Truffle: 18 Benson" /><div class="clear"></div>');
}

function sLinenPolyBlends() {
    $('#linen-poly-blends').append('<img src="v/vspfiles/swatches/linen-poly-blends/light-mauve-whitney.jpg" alt="Light Mauve: Whitney" /><img src="v/vspfiles/swatches/linen-poly-blends/caitlin-putty.jpg" alt="Caitlin Putty" /><div class="clear"></div>');
}

/****** End default swatches ******/




/* Product options below are applied to specific products ***************/
function sWoodFinishes(){
    $('#wood-finishes').append('<img src="v/vspfiles/swatches/wood-finishes/white.jpg" alt="White" /><img src="v/vspfiles/swatches/wood-finishes/antique-white.jpg" alt="Antique White" /><img src="v/vspfiles/swatches/wood-finishes/silver.jpg" alt="silver" /><img src="v/vspfiles/swatches/wood-finishes/weathered-grey.jpg" alt="Weathered Grey" /><img src="v/vspfiles/swatches/wood-finishes/natural.jpg" alt="Natural" /><img src="v/vspfiles/swatches/wood-finishes/blonde.jpg" alt="Blonde" /><img src="v/vspfiles/swatches/wood-finishes/mahogany.jpg" alt="Mahogany" /><img src="v/vspfiles/swatches/wood-finishes/light-meurise.jpg" alt="Light Meurise" /><img src="v/vspfiles/swatches/wood-finishes/espresso.jpg" alt="Espresso" /><img src="v/vspfiles/swatches/wood-finishes/black.jpg" alt="Black" /><div class="clear"></div>');
}


function sMetalFinishes(){
    $('#metal-finishes').append('<img src="v/vspfiles/swatches/metal-finishes/01-mf-chrome.jpg" alt="Chrome" /><img src="v/vspfiles/swatches/metal-finishes/02-mf-brushed-chrome.jpg" alt="Brushed Chrome" /><img src="v/vspfiles/swatches/metal-finishes/03-mf-polished-brass.jpg" alt="Polished Brass" /><img src="v/vspfiles/swatches/metal-finishes/04-mf-brushed-brass.jpg" alt="Brushed Brass" /><div class="clear"></div>');
}


function sNailheadFinishes(){
    $('#nailhead-finishes').append('<img src="v/vspfiles/swatches/nailhead-finishes/old-gold-1009.jpg" alt="Old Gold: 1009" /><img src="v/vspfiles/swatches/nailhead-finishes/french-natural-1009.jpg" alt="French Natural: 1009" /><img src="v/vspfiles/swatches/nailhead-finishes/shiny-nickel-1009.jpg" alt="Shiny Nickel: 1009" /><div class="clear"></div>');
}



/* Large Swatch Thumbnail Effect ***************/

function swThumbEffect(){
//    $('#panel').css('visibility','hidden');


    $('#thumbs img').mousemove(function(event) {
        var mousePositionX = event.pageX-$('#panel').parent().offset().left-100;
        var mousePositionY = event.pageY-$('#panel').parent().offset().top-175;
        $('#panel').css('left', mousePositionX);
        $('#panel').css('top', mousePositionY);
    });

    $('#thumbs img').mouseover(function(){
        $('#largeImage').attr('src',$(this).attr('src'));
        $('#description').html($(this).attr('alt'));
        $('#panel').css('visibility','visible');
    });

    $('#thumbs img').mouseout(function(){
        $('#panel').css('visibility','hidden');
    });
}


/***************************SET UP UNIT OF 2 NEED TO BE SOLD**************************************************/
var twoitems = new Array('CN-E','CN-F','DC-221','DC-231','DC-323B','DC-541','DC-593','DC-606','P002','P022','P055','PC-502A','PC-502B','YS15');
function settwoitems(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('.colors_productprice').append($('<span class="lowerIt"> &nbsp;&nbsp; Price per chair.</span>'));
        $('#tr_qtybtns td:nth-child(1)').hide();
        $('#options_table td[width="100"]').attr('width','45');
        $('#options_table tr:last td:last').append($('<h3 style="padding-left:10px; display:inline-block !important;">This product is sold in units of two</h3>'));
    }
}








/* --------------- Product Details --------------- */


if (location.pathname == "/ProductDetails.asp") {

    $(document).ready(function() {

        // Create new structure for adding content
        $('<table class="mcn-product-info"><tbody><tr><td><h1 class="mcn-product-name"></h1></td></tr></tbody></table>').insertAfter('#v65-product-parent div[itemprop=offers]')

        // Moves product name
        $('td.vCSS_breadcrumb_td font span[itemprop=name]').appendTo($('table.mcn-product-info h1.mcn-product-name'));

        // Moves product price
        $('#v65-product-parent div[itemprop=offers] font div.product_productprice').appendTo($('table.mcn-product-info tr td'));

        // Moves product description
        $('#product_description').appendTo($('table.mcn-product-info tr td'));

        // Moves product dimensions
//        $('div#product-dimensions').appendTo($('table.mcn-product-info tr td'));





        //FUNCTIONS TO TEST FOR SWATCHES
        searchFabrics(allFabrics, global_Current_ProductCode);
        searchNoLeatherettes(noLeatherettes, global_Current_ProductCode);
        searchHarper(swHarper, global_Current_ProductCode);
        searchDelilah(swDelilah, global_Current_ProductCode);
        searchMetalFinishes(swMetalFinishes, global_Current_ProductCode);
        searchNailheadFinishes(swNailheadFinishes, global_Current_ProductCode);
        searchNailheadMetalFinishes(swNailheadMetalFinishes, global_Current_ProductCode);
        searchWoodMetalFinishes(swWoodMetalFinishes, global_Current_ProductCode);
        searchPrints(swPrints, global_Current_ProductCode);
        searchPrintsMetal(swPrintsMetal, global_Current_ProductCode);
        searchPrintsNailhead(swPrintsNailhead, global_Current_ProductCode);
        searchPrintsWood(swPrintsWood, global_Current_ProductCode);
        searchPrintsWoodNailhead(swPrintsWoodNailhead, global_Current_ProductCode);

        //PRODUCTS SOLD IN SETS OF TWO
        settwoitems(twoitems, global_Current_ProductCode);

    });

}


/* -------------- SWATCH REQUEST -------------- */

function validateForm() {
    var x = $(".sr-description input[type=checkbox]:checked").length;
    if (x <= 0 || x > 5) {
        alert("Please select 1 - 5 swatches!");
        return false;
    }
}
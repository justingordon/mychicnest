
// Array of all product codes for the default set of product options (material/finishes)
var allFabrics = new Array('28194Sofa', '28924', '30366Sectional', '30366Sofa', '503', '505', '506', '510', '511', '512', '513', '514', '515', '516', '519', '520', '522', '523', '524', '526', '527', '528', '529', '532', '533', '534', '535', '536', '541', '544', '549', '551', '552', '556', '557', '560', '562', '564', '573', '593', '597', '609', '610', '660', '661', '662', '664', '665', '668', '678', '685', '686', '689', 'FT827', 'FT827ASectional', 'FT827B-Loveseat', 'FT827B-ThreeSeat', 'FT827B-TwoSeat', 'FT828Sectional', 'FT828Sofa', 'FT82A-Fourseat');

function searchFabrics(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();

        swThumbEffect();

    }
}


// Default product options EXCEPT leatherette is missing as the manufacturer can only product it for some tufted products
var noLeatherette = new Array('550', '561', '563');

function searchNoLeatherette(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();

        swThumbEffect();

    }
}



// Default swatches with wood options and nailhead finishes
var swHarper = new Array('502', '538', '600', '601', '604', '605', '607');

function searchHarper(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Wood Finishes</p><div id="wood-finishes"></div><p class="swatchTitle">Nailhead Finishes</p><div id="nailhead-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sWoodFinishes();
        sNailheadFinishes();

        swThumbEffect();

    }
}




// Default swatches with wood options
var swDelilah = new Array('525', '537', '565', '566', '567', '570', '571', '572', '584', '585', '606', '639', '680');

function searchDelilah(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Wood Finishes</p><div id="wood-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sWoodFinishes();

        swThumbEffect();

    }
}


// Default swatches with Metal Finishes
var swMetalFinishes = new Array('');

function searchMetalFinishes(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Metal Finishes</p><div id="metal-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sMetalFinishes();

        swThumbEffect();

    }
}


// Default swatches with Nailhead Finishes
var swNailheadFinishes = new Array('504', '507', '508', '509', '517', '530', '542', '554');

function searchNailheadFinishes(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Nailhead Finishes</p><div id="nailhead-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sNailheadFinishes();

        swThumbEffect();

    }
}

// Default swatches with both Metal and Nailhead Finishes
var swNailheadMetalFinishes = new Array('');

function searchNailheadMetalFinishes(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Metal Finishes</p><div id="metal-finishes"></div><p class="swatchTitle">Nailhead Finishes</p><div id="nailhead-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sMetalFinishes();
        sNailheadFinishes();

        swThumbEffect();

    }
}



// Default swatches with wood and metal finishes
var swWoodMetalFinishes = new Array('');

function searchWoodMetalFinishes(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Wood Finishes</p><div id="wood-finishes"></div><p class="swatchTitle">Metal Finishes</p><div id="metal-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sWoodFinishes();
        sMetalFinishes();

        swThumbEffect();

    }
}



// Default swatches with print fabrics
var swPrints = new Array('518', '521', '531', '539', '540', '543', '553', '555', '558', '559', '574', '575', '576', '577');

function searchPrints(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Prints (additional charge)</p><div id="prints"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sPrints();

        swThumbEffect();

    }
}



// Default swatches with print fabrics and metal finishes
var swPrintsMetal = new Array('500', '569');

function searchPrintsMetal(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Prints (additional charge)</p><div id="prints"></div><p class="swatchTitle">Metal Finishes</p><div id="metal-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sPrints();
        sMetalFinishes();

        swThumbEffect();

    }
}


// Default swatches with print fabrics and nailhead finishes
var swPrintsNailhead = new Array('548');

function searchPrintsNailhead(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Prints (additional charge)</p><div id="prints"></div><p class="swatchTitle">Nailhead Finishes</p><div id="nailhead-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sPrints();
        sNailheadFinishes();

        swThumbEffect();

    }
}




// Default swatches with print fabrics and wood finishes
var swPrintsWood = new Array('568', '583');

function searchPrintsWood(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Prints (additional charge)</p><div id="prints"></div><p class="swatchTitle">Wood Finishes</p><div id="wood-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sPrints();
        sWoodFinishes();

        swThumbEffect();

    }
}



// Default swatches with print fabrics, wood finishes, and nailhead finishes
var swPrintsWoodNailhead = new Array('603');

function searchPrintsWoodNailhead(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('<table id="swatch" class="i_hdash"></table>').insertAfter($('table#i_prodCode'));
        $('table#swatch').append('<tr><td><div id="panel"><img id="largeImage" src="v/vspfiles/swatches/acrylics/01-indigo-1248.jpg" /><div id="description"></div></div><div id="thumbs"><p class="swatchTitle">Woven Polyester</p><div id="woven-polyester"></div><p class="swatchTitle">Cotton-Poly Blends</p><div id="cotton-poly-blends"></div><p class="swatchTitle">Leatherettes</p><div id="leatherettes"></div><p class="swatchTitle">Luxury Velvets (additional charge)</p><div id="luxury-velvets"></div><p class="swatchTitle">Velvets</p><div id="velvets"></div><p class="swatchTitle">Linen Rayon Blend</p><div id="linen-rayon-blend"></div><p class="swatchTitle">Prints (additional charge)</p><div id="prints"></div><p class="swatchTitle">Wood Finishes</p><div id="wood-finishes"></div><p class="swatchTitle">Nailhead Finishes</p><div id="nailhead-finishes"></div></div></td></tr>');


        // Calls functions below for adding swatches to the divs added in the section above
        sWovenPolyester();
        sCottonPolyBlends();
        sLeatherettes();
        sLuxuryVelvets();
        sVelvets();
        sLinenRayonBlend();
        sPrints();
        sWoodFinishes();
        sNailheadFinishes();

        swThumbEffect();

    }
}



/***********************************************************************************************
 Functions of product options (material/finish) to be called after adding the headings (see code above)
 ***********************************************************************************************/


/* Product options below are default options applied to all products ***************/

function sWovenPolyester(){
    $('#woven-polyester').append('<img src="v/vspfiles/swatches/woven-polyester/white-milford-ii.jpg" alt="White Milford II" /><img src="v/vspfiles/swatches/woven-polyester/01-natural-1323.jpg" alt="Natural: 1323" /><img src="v/vspfiles/swatches/woven-polyester/02-oatmeal-1328.jpg" alt="Oatmeal: 1328" /><img src="v/vspfiles/swatches/woven-polyester/almond-bennett-ii.jpg" alt="Almond Bennett II" /><img src="v/vspfiles/swatches/woven-polyester/05-gold-4236.jpg" alt="Gold: 4236" /><img src="v/vspfiles/swatches/woven-polyester/chocolate-marlow-ii.jpg" alt="Chocolate Marlow II" /><img src="v/vspfiles/swatches/woven-polyester/19-coffee-1614.jpg" alt="Coffee: 1614" /><img src="v/vspfiles/swatches/woven-polyester/blue-bird-marlow-ii.jpg" alt="Blue Bird Marlow II" /><img src="v/vspfiles/swatches/woven-polyester/08-sky-1616.jpg" alt="Sky: 1616" /><img src="v/vspfiles/swatches/woven-polyester/09-lake-4227.jpg" alt="Lake: 4227" /><img src="v/vspfiles/swatches/woven-polyester/16-haze-4223.jpg" alt="Haze: 4223" /><img src="v/vspfiles/swatches/woven-polyester/dolphin-marlow-ii.jpg" alt="Dolphin Marlow II" /><img src="v/vspfiles/swatches/woven-polyester/charcoal-milford-ii.jpg" alt="Charcoal Milford II" /><div class="clear"></div>');
}

function sCottonPolyBlends(){$('#cotton-poly-blends').append('<img src="v/vspfiles/swatches/cotton-poly-blends/01-flax-3450.jpg" alt="Flax: 3450" /><img src="v/vspfiles/swatches/cotton-poly-blends/03-sand-chevron-10220-01.jpg" alt="Sand Chevron: 10220-01" /><img src="v/vspfiles/swatches/cotton-poly-blends/05-lavender-10200-14.jpg" alt="Lavender: 10200-14" /><img src="v/vspfiles/swatches/cotton-poly-blends/06-grape-chevron-10220-10.jpg" alt="Grape Chevron: 10220-10" /><img src="v/vspfiles/swatches/cotton-poly-blends/07-pool-10200-10.jpg" alt="Pool: 10200-10" /><img src="v/vspfiles/swatches/cotton-poly-blends/10-grey-chevron-10220-08.jpg" alt="Grey Chevron: 10220-08" /><div class="clear"></div>');
}

function sLeatherettes(){
    $('#leatherettes').append('<img src="v/vspfiles/swatches/leatherettes/01-brilliant-white-7925.jpg" alt="Brilliant White: 7925" /><img src="v/vspfiles/swatches/leatherettes/03-pearl-7017.jpg" alt="Pearl: 7017" /><img src="v/vspfiles/swatches/leatherettes/02-parfait-7359.jpg" alt="Parfait: 7359" /><img src="v/vspfiles/swatches/leatherettes/05-peach-7143.jpg" alt="Peach: 7143" /><img src="v/vspfiles/swatches/leatherettes/08-cashew-7475.jpg" alt="Cashew: 7475" /><img src="v/vspfiles/swatches/leatherettes/09-merigold-7629.jpg" alt="Merigold: 7629" /><img src="v/vspfiles/swatches/leatherettes/10-pink-7281.jpg" alt="Pink: 7281" /><img src="v/vspfiles/swatches/leatherettes/11-ruby-7141.jpg" alt="Ruby: 7141" /><img src="v/vspfiles/swatches/leatherettes/12-marine-7724.jpg" alt="Marine: 7724" /><img src="v/vspfiles/swatches/leatherettes/13-turquoise-7628.jpg" alt="Turquoise: 7628" /><img src="v/vspfiles/swatches/leatherettes/14-kiwi-7269.jpg" alt="Kiwi: 7269" /><img src="v/vspfiles/swatches/leatherettes/16-elephant-7473.jpg" alt="Elephant: 7473" /><img src="v/vspfiles/swatches/leatherettes/17-granite-7136.jpg" alt="Granite: 7136" /><img src="v/vspfiles/swatches/leatherettes/18-saddle-7232.jpg" alt="Saddle: 7232" /><div class="clear"></div>');
}

function sLuxuryVelvets(){
    $('#luxury-velvets').append('<img src="v/vspfiles/swatches/luxury-velvets/01-ivory-10150-15.jpg" alt="Ivory: 10150-15" /><img src="v/vspfiles/swatches/luxury-velvets/02-clay-10150-13.jpg" alt="Clay: 10150-13" /><img src="v/vspfiles/swatches/luxury-velvets/03-mocha-10150-19.jpg" alt="Mocha: 10150-19" /><img src="v/vspfiles/swatches/luxury-velvets/04-paprika-10150-06.jpg" alt="Paprika: 10150-06" /><img src="v/vspfiles/swatches/luxury-velvets/05-fuchsia-10150-08.jpg" alt="Fuchsia: 10150-08" /><img src="v/vspfiles/swatches/luxury-velvets/07-rhubarb-10150-18.jpg" alt="Rhubarb: 10150-18" /><img src="v/vspfiles/swatches/luxury-velvets/08-peacock-10150-02.jpg" alt="Peacock: 10150-02" /><img src="v/vspfiles/swatches/luxury-velvets/09-blue-smoke-10150-12.jpg" alt="Blue Smoke: 10150-12" /><img src="v/vspfiles/swatches/luxury-velvets/10-moss-10150-04.jpg" alt="Moss: 10150-04" /><img src="v/vspfiles/swatches/luxury-velvets/11-hunter-10150-14.jpg" alt="Hunter: 10150-14" /><img src="v/vspfiles/swatches/luxury-velvets/12-mineral-10150-07.jpg" alt="Mineral: 10150-07" /><img src="v/vspfiles/swatches/luxury-velvets/13-mulberry-10150-09.jpg" alt="Mulberry: 10150-09" /><img src="v/vspfiles/swatches/luxury-velvets/14-plum-10150-20.jpg" alt="Plum: 10150-20" /><img src="v/vspfiles/swatches/luxury-velvets/15-bark-10150-01.jpg" alt="Bark: 10150-01" /><img src="v/vspfiles/swatches/luxury-velvets/16-black-10150-03.jpg" alt="Black: 10150-03" /><div class="clear"></div>');
}

function sVelvets(){
    $('#velvets').append('<img src="v/vspfiles/swatches/velvets/vanilla-1-benson.jpg" alt="Vanilla: 1 Benson" /><img src="v/vspfiles/swatches/velvets/mauve-33-benson.jpg" alt="Mauve: 33 Benson" /><img src="v/vspfiles/swatches/velvets/02-carnation-4880.jpg" alt="Carnation: 4880" /><img src="v/vspfiles/swatches/velvets/03-rose-10300-17.jpg" alt="Rose: 10300-17" /><img src="v/vspfiles/swatches/velvets/07-wave-10300-28.jpg" alt="Wave: 10300-28" /><img src="v/vspfiles/swatches/velvets/capri-36-benson.jpg" alt="Capri: 36 Benson" /><img src="v/vspfiles/swatches/velvets/shadow-empire-iii.jpg" alt="Shadow: Empire III" /><img src="v/vspfiles/swatches/velvets/09-navy-10300-23.jpg" alt="Navy: 10300-23" /><img src="v/vspfiles/swatches/velvets/12-platinum-4895.jpg" alt="Platinum: 4895" /><img src="v/vspfiles/swatches/velvets/dove-6-benson.jpg" alt="Dove: 6 Benson" /><img src="v/vspfiles/swatches/velvets/latte-25-benson.jpg" alt="Latte: 25 Benson" /><img src="v/vspfiles/swatches/velvets/11-pewter-4891.jpg" alt="Pewter: 4891" /><img src="v/vspfiles/swatches/velvets/truffle-18-benson.jpg" alt="Truffle: 18 Benson" /><div class="clear"></div>');
}

function sLinenRayonBlend() {
    $('#linen-rayon-blend').append('<img src="v/vspfiles/swatches/linen-rayon-blend/light-mauve-whitney.jpg" alt="Light Mauve: Whitney" /><div class="clear"></div>');
}

/****** End default swatches ******/




/* Product options below are applied to specific products ***************/
function sWoodFinishes(){
    $('#wood-finishes').append('<img src="v/vspfiles/swatches/wood-finishes/white.jpg" alt="White" /><img src="v/vspfiles/swatches/wood-finishes/antique-white.jpg" alt="Antique White" /><img src="v/vspfiles/swatches/wood-finishes/silver.jpg" alt="silver" /><img src="v/vspfiles/swatches/wood-finishes/weathered-grey.jpg" alt="Weathered Grey" /><img src="v/vspfiles/swatches/wood-finishes/natural.jpg" alt="Natural" /><img src="v/vspfiles/swatches/wood-finishes/blonde.jpg" alt="Blonde" /><img src="v/vspfiles/swatches/wood-finishes/mahogany.jpg" alt="Mahogany" /><img src="v/vspfiles/swatches/wood-finishes/light-meurise.jpg" alt="Light Meurise" /><img src="v/vspfiles/swatches/wood-finishes/espresso.jpg" alt="Espresso" /><img src="v/vspfiles/swatches/wood-finishes/black.jpg" alt="Black" /><div class="clear"></div>');
}


function sMetalFinishes(){
    $('#metal-finishes').append('<img src="v/vspfiles/swatches/metal-finishes/01-mf-chrome.jpg" alt="Chrome" /><img src="v/vspfiles/swatches/metal-finishes/02-mf-brushed-chrome.jpg" alt="Brushed Chrome" /><img src="v/vspfiles/swatches/metal-finishes/03-mf-polished-brass.jpg" alt="Polished Brass" /><img src="v/vspfiles/swatches/metal-finishes/04-mf-brushed-brass.jpg" alt="Brushed Brass" /><div class="clear"></div>');
}


function sNailheadFinishes(){
    $('#nailhead-finishes').append('<img src="v/vspfiles/swatches/nailhead-finishes/old-gold-1009.jpg" alt="Old Gold: 1009" /><img src="v/vspfiles/swatches/nailhead-finishes/french-natural-1009.jpg" alt="French Natural: 1009" /><img src="v/vspfiles/swatches/nailhead-finishes/shiny-nickel-1009.jpg" alt="Shiny Nickel: 1009" /><div class="clear"></div>');
}


function sPrints(){
    $('#prints').append('<img src="v/vspfiles/swatches/prints/01-blue-flowers-20480-02.jpg" alt="Blue Flowers: 20480-02" /><img src="v/vspfiles/swatches/prints/02-floral-garden-20460-01.jpg" alt="Floral Garden: 20460-01" /><img src="v/vspfiles/swatches/prints/03-blue-butterfly-20470-03.jpg" alt="Blue Butterfly: 20470-03" /><img src="v/vspfiles/swatches/prints/04-pink-butterfly-20470-01.jpg" alt="Pink Butterfly: 20470-01" /><img src="v/vspfiles/swatches/prints/05-ikat-10170-02.jpg" alt="Ikat: 10170-02" /><img src="v/vspfiles/swatches/prints/06-large-pink-florals-10166-01.jpg" alt="Large Pink Florals: 10166-01" /><img src="v/vspfiles/swatches/prints/07-large-green-florals-10166-04.jpg" alt="Large Green Florals: 10166-04" /><img src="v/vspfiles/swatches/prints/leopard-I9400-15.jpg" alt="Leopard: I9400-15" /><div class="clear"></div>');
}




/* Large Swatch Thumbnail Effect ***************/

function swThumbEffect(){
//    $('#panel').css('visibility','hidden');


	$('#thumbs img').mousemove(function(event) {
		var mousePositionX = event.pageX-$('#panel').parent().offset().left-100;
		var mousePositionY = event.pageY-$('#panel').parent().offset().top-175;
		$('#panel').css('left', mousePositionX);
		$('#panel').css('top', mousePositionY);
	});

    $('#thumbs img').mouseover(function(){
        $('#largeImage').attr('src',$(this).attr('src'));
        $('#description').html($(this).attr('alt'));
        $('#panel').css('visibility','visible');
    });
	
    $('#thumbs img').mouseout(function(){
        $('#panel').css('visibility','hidden');
    });
}


/***************************SET UP UNIT OF 2 NEED TO BE SOLD**************************************************/
var twoitems = new Array('CN-E','CN-F','DC-221','DC-231','DC-323B','DC-541','DC-593','DC-606','P002','P022','P055','PC-502A','PC-502B','YS15');
function settwoitems(ArrayObj, SearchFor){
    var Found = false;
    for (var i = 0; i < ArrayObj.length; i++){
        if (ArrayObj[i] == SearchFor){
            //return true;
            var Found = true;
            break;
        }
    }
    if( Found == true){
        $('.colors_productprice').append($('<span class="lowerIt"> &nbsp;&nbsp; Price per chair.</span>'));
        $('#tr_qtybtns td:nth-child(1)').hide();
        $('#options_table td[width="100"]').attr('width','45');
        $('#options_table tr:last td:last').append($('<h3 style="padding-left:10px; display:inline-block !important;">This product is sold in units of two</h3>'));
    }
}

/********************************* ABOUT US ********************************/
function writeRight(){ //Creates 3 vertical boxes on the right side of the content area
    $('<div class="right"><a href="http://www.mychicnest.com/aboutus.asp" class="r1">\n<h2><img src="v/vspfiles/templates/00_whiteLt/img/productHome/arrow.png" class="arrow"/>welcome to MY CHIC NEST</h2>\n<p>Our furniture offers a new, accessible way to make the vision for your home a reality. </p>\n</a>\n<a href="http://www.mychicnest.com/SearchResults.asp?Cat=1817" class="r2">\n<h2><img src="v/vspfiles/templates/00_whiteLt/img/productHome/arrow.png" class="arrow"/>DECOR  for the masses</h2>\n<p>We feel that style and good design do not have to cost a fortune, and our collection is proof.</p>\n</a>\n<a href="http://www.mychicnest.com/Articles.asp?ID=57" class="r3">\n<h2><img src="v/vspfiles/templates/00_whiteLt/img/productHome/arrow.png" class="arrow"/>FREE shipping</h2>\n<p>We avoid middle-men altogether, so all our products come with absolutely free shipping.</p>\n</a>\n<div class="shadow_btm"></div>\n<div class="clear"></div>\n</div>\n<div class="clear"></div>').insertAfter($('#content_area .left'));
}




/***************************** ONE PAGE CHECKOUT ??? ****************************/
$(document).ready(function() {

	// These appear to be used on one-page-checkout.asp
    $('img[src="v/vspfiles/templates/00_whiteLt/images/Icon_FreeShipping_Small.gif"]').hide();
    $('input[name$="btnApply"]').attr('align','top');
    $('input[name$="btnApply"]').css('margin-left','5px');

//ERROR STATE!!
    $('img[src$="/a/i/error_alert.gif"]').parent().remove();
    $('td[bgcolor="#FFFFCC"]').css({'padding-top':'10px'});
    $('td[bgcolor="#FFFFCC"]').attr('width','912px');
    $('table[BGCOLOR="#FFFFCC"]').addClass('errorstate');
    $('table#pci-errors').attr('width','100%');
    $('table#pci-errors').css({'padding-top':'10px'});

    $('table.errorstate h2').css({'display':'inline'});
    $('table.errorstate span.optform').css('font-size','12px');
    $('table.errorstate').attr('width','912px');

});




/* --------------- Product Category --------------- */
if (location.pathname == "/SearchResults.asp") {
    document.writeln("\n<style type='text/css'>#content_area table tbody tr td.colors_backgroundneutral{display:none;} table tr td table tr td a:hover{text-decoration:none; color:#000000;} .colors_lines_light{background-color:#ffffff !important; padding-bottom:10px !important;}.v65-productDisplay td{background-position:left bottom; background-repeat:repeat-x;} #MainForm table{border-spacing:0; border-collapse:collapse; padding:0px !important;} #MainForm {width:912px !important;} form.search_results_section table.v65-productDisplay a.productnamecolor{padding-top:10px !important;}</style>\n");
    $('#MainForm table:nth-child(2) table.v65-productDisplay table.v65-productDisplay tr:nth-child(1) td').css('background-image', 'none');
    $(document).ready(function() {
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/clear1x1.gif"][width="25"][height="25"]').css('width','1px');
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/clear1x1.gif"][width="25"][height="25"]').css('height','25px');
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/Icon_New.gif"][javascript:void(0);border="0"]').parent().addClass('eachprod');
        $('.eachprod').filter(function() {
            $('a', this).prependTo($(this));
        });
        $('td[background="v/vspfiles/templates/00_whiteLt/images/Grid_Divider_Vertical.gif"]').css('padding-right','10px');
        $('.previous_page_img').css('padding-bottom','10px');
        $('.next_page_img').css('padding-bottom','10px');
        $('.next_page_img').css('padding-left','2px');
        $('img[width="3"][height="9"]').parent().css({'background-position':'0px 15px', 'height':'30px'});
        $('img[width="2"][height="1"]').parent().remove();
        $('img[width="3"][height="1"]').parent().remove();
        $('#MainForm table').attr('cellpadding','0');
        $('form.search_results_section table.v65-productDisplay td[width="25%"]').attr('align','left');
    });
}

/* --------------- Login --------------- */

/*
if (location.pathname == "/login.asp") {
    document.writeln("\n<style type='text/css'> #content_area{padding-bottom:20px;} #content_area br{display:none;} p{padding-top:5px; padding-bottom:15px;} #content_area td{text-align:left !important; vertical-align:text-top;} #content_area td li{list-style:none;} #content_area td input[type=password], #content_area td input[type=text]{margin-bottom:10px; width:165px;} #content_area table tr td table tr td table tr td{vertical-align:top !important; min-height:22px; padding-bottom:10px;} #content_area table tr td table tr td table{background-color:#fff;} #content_area table tr td table tr td table td img{display:none;} body{background-color:#bdbbc0; background-image:url(v/vspfiles/templates/00_whiteLt/img/productCategory/bg2.jpg); } .colors_lines_light{background-color:#dddddd !important;} .errorstate td{padding-bottom:0px !important;} </style>\n");

    $(document).ready(function() {
        $('<h1>Login</h1>').insertBefore($('img[src$="v/vspfiles/templates/00_whiteLt/images/headings/heading_login.gif"]'));
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/headings/heading_login.gif"]').remove();
        $('form[name="loginform"]').css('margin-top','15px');
        $('h3:contains("Returning Customers")').parent().parent().parent().parent().css('padding-top','20px');
    });
}
*/


/* --------------- Product Details --------------- */


if (location.pathname == "/ProductDetails.asp") {

    $(document).ready(function() {
        $('.vCSS_breadcrumb_td br').remove(); //I think this can be removed
        $('.v65-productColumn-divider').css({'background-repeat': 'repeat-x', 'background-position': 'bottom left'}); // Adjusts border in "You may also like" section

//MOVING PRODUCT TITLE
        $('.product_code_title').parents('i').prevAll('br').hide();
        $('.productnamecolorLARGE').insertBefore($('.pricecolor').parent());
        $('.productnamecolorLARGE').parent().parent().parent().parent().addClass('i_hdash'); // Adds class to table containing product code
        $('.productnamecolorLARGE').parent().parent().parent().parent().attr('id', 'i_topInfo'); // Adds ID to div containing product name
        $('<table class="i_hash" id="i_prodCode"><tr><td id="i_code"></td></tr></table>').insertAfter($('#i_topInfo')); //Adds product code after div with product name

//MOVING PRODUCT PRICE
		$('font.productnamecolorLARGE').next('td').attr('id', 'product-price-cell').insertAfter($('font.productnamecolorLARGE').parent());
		$('td#product-price-cell').wrap('<tr></tr>');
		$('td#product-price-cell').attr('width', '300');
		
//MOVE PRODUCT CODE AFTER PRODUCT PRICE (SAME ROW)
		$('td#i_code').append($('.product_code_title').parent()); // Adds spans with product code to td#i_code
		$('td#i_code').insertAfter('td#product-price-cell');
				
//MOVE PRODUCT DESCRIPTION & DIMENSIONS
		$('div#product-description').insertAfter($('#i_topInfo'));

//		$('div#lead-time').insertAfter($('div#product-description'));   //original
//      $('div#product-dimensions').insertAfter($('div#lead-time'));    //original

        $('div#product-dimensions').insertAfter($('div#product-description'));


		
//ADD DIVIDER AND SWATCH OPTION TITLE
		$('<hr class="product-option-hr" />').insertAfter('div#product-dimensions');
		
		
//SETUP OPTIONS AREA
        $('<div id="product-fabric-options"><h2>1. SELECT FABRIC OPTIONS</h2><a href="/Articles.asp?ID=279" target="_blank"><img src="v/vspfiles/templates/mcn_2015/images/buttons/btn-request-swatches.gif" alt="Request Swatches" /></a></div>').insertAfter('hr.product-option-hr');



//ADD LEAD TIME DYNAMICALLY -- this needs to be fixed
        $('div#lead-time').insertAfter($('div#product-dimensions'));
        $('#dynamic-lead-time').text('Expected Lead Time: 6-8 weeks');


//MOVING PRODUCT DETAILS
        $('#ProductDetail_ProductDetails_div').parent().parent().parent().parent().remove(); //Removes product details
        $('table.colors_lines_light').insertAfter($('form#vCSS_mainform'));
        $('#Header_ProductDetail_ProductDetails').parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().remove();

//MOVING QUANTITY AND BUTTONS
        $('h3:contains("Qty")').parent().parent().attr('id','tr_qtybtns');
        $('#v65-product-wishlist-button').insertAfter($('.vCSS_input_addtocart'));
        $('<table id="qtybtns"></table>').insertAfter($('table#i_prodCode'));
        $('table#qtybtns').append($('tr#tr_qtybtns'));

//PREPARING OPTIONS AREA
        $('table#options_table').parent().parent().parent().parent().attr('id','wholeOpt');
        $('table#wholeOpt br').remove();
        $('table#wholeOpt').insertAfter($('table#i_prodCode'));
        $('table#wholeOpt td').attr('align','left');
        //$('table#options_table td:nth-child(2)').remove();
        //$('table#options_table td:nth-child(1)').attr('width','100');
        $('table#options_table td img[src$="v/vspfiles/templates/00_whiteLt/images/clear1x1.gif"]').remove();
        $('table#options_table td:contains("White Glove Delivery")').css('padding-bottom', '10px');
        $('td font:contains(Click to view another Color)').hide();

//ADDING V-LINE TO SEPARATE IMAGES FROM CONTENT
        $('.colors_pricebox').parent().css('background-image','url(v/vspfiles/templates/00_whiteLt/images/content_mid.gif)');
        $('.colors_pricebox').parent().css('background-repeat','repeat-y');
        $('.colors_pricebox').parent().css('background-position', 'left top');
        $('.colors_pricebox').parent().css('padding-left', '8px');
        $('.colors_pricebox').parent().css('width', '50%');

//SOCIAL STUFF
        $('<table id="i_socialStuff"><tr><td></td></tr></table>').insertAfter($('table#qtybtns'));
        $('#v65-share-buttons-cell').parent().parent().parent().attr('id', 'i_social');
        $('table#i_socialStuff tr td').append($('#i_social'));

        $('span.PageText_L498n').hide();
        $('span.PageText_L668n').hide();

        $('#v65-product-parent tr:eq(1) td:eq(0)').css('width','50%');

        $('.vCSS_img_icon_free_shipping').parent().hide();
        $('#altviews img').attr('align','left');


//TEMP
	$('td#tr_qtybtns').parent().attr('height', '75');

		

//FUNCTIONS TO TEST FOR SWATCHES
        searchFabrics(allFabrics, global_Current_ProductCode);
        searchNoLeatherette(noLeatherette, global_Current_ProductCode);
        searchHarper(swHarper, global_Current_ProductCode);
        searchDelilah(swDelilah, global_Current_ProductCode);
        searchMetalFinishes(swMetalFinishes, global_Current_ProductCode);
        searchNailheadFinishes(swNailheadFinishes, global_Current_ProductCode);
        searchNailheadMetalFinishes(swNailheadMetalFinishes, global_Current_ProductCode);
        searchWoodMetalFinishes(swWoodMetalFinishes, global_Current_ProductCode);
        searchPrints(swPrints, global_Current_ProductCode);
        searchPrintsMetal(swPrintsMetal, global_Current_ProductCode);
        searchPrintsNailhead(swPrintsNailhead, global_Current_ProductCode);
        searchPrintsWood(swPrintsWood, global_Current_ProductCode);
        searchPrintsWoodNailhead(swPrintsWoodNailhead, global_Current_ProductCode);

//PRODUCTS SOLD IN SETS OF TWO
        settwoitems(twoitems, global_Current_ProductCode);

//TEST LEADTIME
        setLeadTime(global_Current_ProductCode);




    });

}


/* -------------- SWATCH REQUEST -------------- */

function validateForm() {
    var x = $(".sr-description input[type=checkbox]:checked").length;
    if (x <= 0 || x > 4) {
        alert("Please select 1 - 4 swatches!");
        return false;
    }
}




/* --------------- My Account --------------- */

/*
if (location.pathname == "/myaccount.asp") {
    document.writeln("\n<style type='text/css'>p{font-size:12px !important; padding-bottom:20px;} #content_area table{width:100% !important;} #content_area form{background:url(../img/template/hline_dash.png) top left repeat-x;} #content_area table td{text-align:left;} #content_area .colors_lines_light {background-color:none;} body{background-color:#bdbbc0; background-image:url(v/vspfiles/templates/00_whiteLt/img/productCategory/bg2.jpg);} #content_area{padding-bottom:15px;}</style>\n");
}
*/

/* --------------- Mailing list --------------- */

/*
if (location.pathname == "/MailingList_subscribe.asp") {
    document.writeln("\n<style type='text/css'>p{font-size:12px !important; padding-bottom:20px;} #content_area table{width:100% !important;} #content_area table td{text-align:left;} #content_area .colors_lines_light {background-color:none;} body{background-color:#bdbbc0; background-image:url(v/vspfiles/templates/00_whiteLt/img/productCategory/bg2.jpg);}  #content_area br{display:none;} </style>\n");

    $(document).ready(function() {
        $('tr.colors_backgroundneutral td:first-child').attr('width', 25);
        $('<h1>MAILING LIST</h1>').insertAfter($('img[src$="v/vspfiles/templates/00_whiteLt/images/headings/heading_MailingList.gif"]'));
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/headings/heading_MailingList.gif"]').remove();



    });

}


if (location.pathname == "/MailingList_unsubscribe.asp" || location.pathname == "/MyAccount_GiftBalance.asp" || location.pathname == "/MyAccount_ApplyGift.asp"  || location.pathname == "/login_sendpass.asp"){
    document.writeln("\n<style type='text/css'>#content_area br{display:none;} a.rollit{display:none !important;} </style>\n");
    $(document).ready(function() {
        $('#content_area td').attr('align','left');
        $('#content_area table').attr('width','100%');
        $('#content_area td[width="91"]').attr('width','50');

    });
}
*/

/* --------------- help --------------- */

/*
if (location.pathname == "/help.asp") {
    document.writeln("\n<style type='text/css'>body{line-height:20px;} p{font-size:12px !important; padding-bottom:20px;} #content_area table{width:100% !important;} #content_area table td{text-align:left;} #content_area .colors_lines_light {background-color:none;}  .colors_productname{padding-top:15px;}</style>\n");

    $(document).ready(function() {
        $('.colors_productname').next('br').hide();
        $('tr[valign="top"] td:last table').append($('font:contains(ORDERS)').parent().parent());

    });
}
/*

/* --------------- help answers --------------- */

/*
if (location.pathname == "/help_answer.asp") {
    document.writeln("\n<style type='text/css'>body{line-height:18px;} td{font-family: 'Lato', Helvetica, Arial, sans-serif !important; font-style: normal; font-weight: 300; font-size: 12px !important;} td a{text-decoration:none; color:#000000; } td a:link, td a:visited{ text-decoration:none;} td a:hover, td a:active{text-decoration:underline; color:#000000;} .productnamecolorLARGE{text-transform:none;} strong{font-size:12px !important;} .rollit{display:none !important;} #content_area{border-bottom:none !important;} #leftarea{padding-bottom:15px; } #leftnorm{position:relative;} body{background-color:#785800; background-image:url(v/vspfiles/templates/00_whiteLt/img/productCategory/bg3.jpg); } table.infohere td{padding-left:235px !important;} #a_infoArea p{padding:0px 45px 0px 0px;} strong{font-style:normal; font-weight:normal;}</style>\n");

    $('html').scrollTop(0);
    $(document).ready(function() {
        $('#content_area table').attr({cellpadding:'0', cellspacing:'0'});
        $('td[width="15"]').attr('width','1').css('display','none');
        $('img[width="3"][height="9"]').remove();
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/headings/heading_help.gif"]').remove();
        $('#content_area table').addClass('infohere');
        $('#content_area').prepend($('<div class="left" id="leftnorm"><table id="leftarea"><tr><td class="a_long" id="a_infoArea"></td></tr></table><a href="http://www.mychicnest.com/ProductDetails.asp?ProductCode=538" id="a_faq"><img src="v/vspfiles/templates/00_whiteLt/images/infoGallery/faq_product_harper.jpg" /><span class="faq_caption">HARPER (details)</span></a></div>'));
        $('.a_long').append($('.infohere'));
        $('#content_area br').remove();
        writeRight();

    });
}
*/

/* --------------- articles --------------- */

/*
if (location.pathname == "/Articles.asp"|| location.pathname == "/terms_privacy.asp") {
    document.writeln("\n<style type='text/css'>body{line-height:18px;} td{font-family: 'Lato', Helvetica, Arial, sans-serif !important; font-style: normal; font-weight: 300; font-size: 12px !important;} td a{text-decoration:none; color:#000000; } td a:link, td a:visited{ text-decoration:none;} td a:hover, td a:active{text-decoration:underline; color:#000000;} .productnamecolorLARGE{text-transform:none;} strong{font-size:12px !important;} .rollit{display:none !important;} #content_area{border-bottom:none !important;} #leftarea{padding-bottom:15px;} #leftnorm{position:relative;} body{background-color:#785800; background-image:url(v/vspfiles/templates/00_whiteLt/img/productHome/bg1.jpg); } </style>\n");

    $('html').scrollTop(0);
    $(document).ready(function() {
        $('td[width="15"]').attr('width','0').css('display','none');
        $('img[width="3"][height="9"]').remove();
        $('img[height="30"]').remove();
        $('#content_area table').addClass('infohere');
        $('#content_area').prepend($('<div class="left" id="leftnorm"><table id="leftarea"><tr><td class="a_blank" id="a_infoArea"></td></tr></table></div>'));
        $('.a_blank').append($('.infohere'));
        $('#content_area br').remove();
        if (location.pathname != "/Articles.asp?ID=266"){
            writeRight();
        }

    });
}
*/

/* --------------- TERMS AND CONDITIONS --------------- */

/*
if (location.pathname == "/terms.asp") {
    document.writeln("\n<style type='text/css'>body{line-height:18px;} td{font-family: 'Lato', Helvetica, Arial, sans-serif !important; font-style: normal; font-weight: 300; font-size: 12px !important;} td a{text-decoration:none; color:#000000; } td a:link, td a:visited{ text-decoration:none;} td a:hover, td a:active{text-decoration:underline; color:#000000;} .productnamecolorLARGE{text-transform:none;} strong{font-size:12px !important;} .rollit{display:none !important;} #content_area{border-bottom:none !important;} #leftarea{padding-bottom:15px;} #leftnorm{position:relative;} body{background-color:#785800; background-image:url(v/vspfiles/templates/00_whiteLt/img/productHome/bg1.jpg); } table.infohere td{padding-left:235px !important;} </style>\n");

    $('html').scrollTop(0);
    $(document).ready(function() {
        $('td[width="15"]').attr('width','0').css('display','none');
        $('img[width="3"][height="9"]').remove();
        $('img[height="30"]').remove();
        $('#content_area table').addClass('infohere');
        $('#content_area').prepend($('<div class="left" id="leftnorm"><table id="leftarea"><tr><td class="a_long" id="a_infoArea"></td></tr></table></div>'));
        $('.a_long').append($('.infohere'));
        $('#content_area br').remove();
        writeRight();


    });
}
*/

/* --------------- orders --------------- */

/*
if (location.pathname == "/orders.asp") {
    document.writeln("\n<style type='text/css'> td{font-family: 'Lato', Helvetica, Arial, sans-serif !important; font-style: normal; font-weight: 300; font-size: 12px !important; vertical-align:center;} tr.dselect td{padding:10px 0px 15px;} .tableTitle{font-weight:700 !important; text-transform:uppercase;} .colors_background3{background-color:#ffffff; border-top:1px dotted #afafaf; padding:5px 0px; margin-bottom:5px;} #content_area br{display:none;} .rollit{display:none !important;} .colors_lines_light ul{padding-left:0px !important;} tr.colors_backgroundneutral td{padding-top:0px} .colors_background3_text{margin-bottom:10px !important;} .PageText_L719n{display:block;} #content_area{padding-bottom:15px;}</style>\n");

    $(document).ready(function() {
        $('a:contains("Return to My Account")').remove();
        $('<h1>MY ACCOUNT <a href="myaccount.asp" class="otherlinks">Return to My Account ></a></h1>').insertAfter($('img[src$="v/vspfiles/templates/00_whiteLt/images/headings/heading_MyAccount.gif"]'));

        $('input[src="v/vspfiles/templates/00_whiteLt/images/Template/btn_go.gif"]').parent().addClass('hiding');

        $('b:contains(ORDER HISTORY)').parent().css('padding','20px 0px');
        $('table.colors_lines_light').parent().css('padding','10px 0px');
        $('table.colors_lines_light tr.colors_backgroundneutral br').css('display','block');

        $('td:contains(My Orders)').text().replace('My Orders', '<h1>My Orders</h1>');

        $('<tr><td colspan="3"><img src="v/vspfiles/templates/00_whiteLt/images/clear1x1.gif" height="20" width="10" /></td></tr>').insertBefore($('tr.colors_backgroundneutral'));
//$('tr.colors_backgroundneutral td').attr('align','left');
        $('tr.colors_backgroundneutral td:contains(Items Ordered)').attr('width','320');

    });
}

if (location.pathname == "/returns.asp") {
    document.writeln("\n<style type='text/css'>#content_area br, .rollit{display:none;} #content_area input[type=text]{margin-right:5px;} #content_area{padding-bottom:25px;} </style>");
}

if (location.pathname == "/returns.asp" || location.pathname == "/ChangeOrderCancelItem.asp") {
    document.writeln("\n<style type='text/css'>#content_area br, .rollit{display:none;} #content_area input[type=text]{margin-right:5px;} #content_area{padding-bottom:25px;} </style>");
    $(document).ready(function() {
    });
}
*/

/* --------------- ORDER DETAILS --------------- */

/*
if (location.pathname == "/orderdetails.asp"  || location.pathname == "/OrderDetails.asp") {
    document.writeln("\n<style type='text/css'> td{font-family: 'Lato', Helvetica, Arial, sans-serif !important; font-style: normal; font-weight: 300; font-size: 12px !important; vertical-align:center;} tr.dselect td{padding:10px 0px 15px;} .tableTitle{font-weight:700 !important; text-transform:uppercase;} .rollit{display:none !important;} .colors_lines_light ul{padding-left:0px !important;} tr.colors_backgroundneutral td{padding-top:0px} .colors_background3_text{margin-bottom:10px !important;} #content_area h3, strong{font-size:12px !important; font-weight:700 !important; display:inline;} .colors_background3_text{border:none !important;} .changeit{padding: 3px 0px; margin-bottom: 10px; margin-top: 10px; border-top: 1px dotted #AFAFAF; border-bottom: 1px dotted #AFAFAF; display: block; }.changeit .colors_background3_text b{font-family: 'Lato', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: 300 !important; font-size: 14px !important; height: inherit; line-height: 18px; color: #6D6E71; text-transform: uppercase;} .barit td{height:1px !important; background-color:#ffffff; border-top:1px solid #afafaf;} tr.colors_backgroundneutral td{color: #6D6E71;}</style>\n");

    $(document).ready(function() {
        $('td.bgcolor[colspan="3"]').css({'border-top':'1px dotted #afafaf','border-bottom':'1px dotted #afafaf'});
        $('<h1>Order Details</h1>').insertAfter($('b:contains("Order Details")'));
        $('b:contains("Order Details")').remove();
        $('h1').parent().attr('align','left');
        $('b:contains("Payment Information")').parent().parent().parent().parent().addClass('changeit');
        $('h3:contains("Order Status")').parent().parent().parent().css({'border-top':'1px solid #afafaf', 'padding':'5px 0px'});
        $('.changeit td[width="46%"]').attr('width','90%');
        $('.changeit td[width="54%"]').attr('width','10%');
        $('b:contains("Bill To:")').css('font-weight','700');
        $('td[bgcolor="#999999"]').parent().addClass('barit');
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_changeqty.gif"]').css('padding-bottom','10px');
        $('<span>Change ></span>').insertAfter($('img[src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_change.gif"]'));
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_change.gif"]').parent().addClass('redlink');
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_change.gif"]').remove();
    });
}
*/

/* --------------- GO BACK IMAGE --------------- */

/*
$(document).ready(function() {
    $('img[src$="v/vspfiles/templates/00_whiteLt/images/Bullet_GoBack.gif"]').next('a').addClass('rollit');
    $('img[src$="v/vspfiles/templates/00_whiteLt/images/Bullet_GoBack.gif"]').remove();
});
*/

/* --------------- SHOPPING CART --------------- */

/*
if (location.pathname == "/ShoppingCart.asp" || location.pathname == "/shoppingcart.asp") {document.writeln("\n<link href='v/vspfiles/templates/00_whiteLt/css/shoppingcart.css' rel='stylesheet' type='text/css' />\n");
    document.writeln("\n<style type='text/css'>body{background-color:#785800; background-image:url(v/vspfiles/templates/00_whiteLt/img/productCategory/bg3.jpg);}</style>\n");

    $(document).ready(function() {
        $('.v65-post-breadcrumb-br').remove();
        $('#v65-cart-moreItems br').remove();

        $('.v65-cart-tax-parent-cell td[width="135"]').attr('width','115');
        $('.v65-cart-tax-parent-cell td[width="10"]').attr('width','20');
        $('#v65-cart-total-estimate td[width="135"]').attr('align','center');

        $('font.colors_productprice b').css('font-weight','300');
        $('td#v65-cart-post-coupon-blank').next('td').css('padding-right','20px');
        $('font.carttext').removeClass('colors_text').addClass('newCartText');
        $('font.newCartText').removeClass('carttext');

        $('a.colors_productname').removeClass('carttext').addClass('newCartText');
        $('.newCartText b').contents().unwrap();

        $('.newCartText').parent().attr('align', 'center');
        $('.newCartText').parent().css('padding-top','25px');
        $('.v65-cart-details-row td input').parent().css('padding-top','25px');

        $('tr.v65-cart-tax-row').prev('tr').hide();

        $('input[type="image"][src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_apply.gif"]').css('vertical-align','top');

        $('<tr class="v65-cart-tax-row2"><td colspan="6"></td><td colspan="3" class="taxtitle" style="padding-right:20px;"><div align="right"><b>Tax:&nbsp;</b></div></td><td colspan="2"></td><tr>').insertAfter($('.v65-cart-tax-row'));
        $('tr.v65-cart-tax-row td.v65-cart-tax-parent-cell').insertAfter($('tr.v65-cart-tax-row2 td.taxtitle'));
        $('tr.v65-cart-tax-row').hide();

        $('br.beforeArticleBR').remove();

        $('table#v65-cart-moreItems').attr('width','100%');
        $('table#v65-cart-checkout-table').attr('width','100%');
        $('tr.v65-cart-checkout-row td:nth-child(1)').attr('width','48%');
        $('tr.v65-cart-checkout-row td:nth-child(2)').attr('width','4%');
        $('tr.v65-cart-checkout-row td:nth-child(3)').attr('width','48%');

        $('td.v65-productName').attr('align','left');
        $('table.v65-productDisplay td.v65-productName a').removeClass('newCartText productnamecolor colors_productname').addClass('productnamecolorsmall colors_productname');

        $('img[src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_addtocart_small.gif"]').css('padding','5px 0px');

        $('td#v65-checkout-details-guest').append($('input.btn_checkout_guest'));
        $('<br>').insertBefore($('input.btn_checkout_guest'));
        $('h3:contains("Total")').css({'display':'inline','font-weight':'700'});

        $('#v65-cart-checkout-table').parent().parent().addClass('checkoutarea1');
        $('.checkoutarea1').css({'border-top':'1px dotted #afafaf', 'border-bottom':'1px dotted #afafaf', 'padding-top':'20px', 'padding-bottom':'20px'});
        $('.checkoutarea1 br').remove();
        $('.checkoutarea1 img').css('display','block');

        $('tr.v65-cart-giftcert-details-row').prev('tr').addClass('wantRemove');
        $('.wantRemove td').css('border-top','none');

    });
}

*/

/* --------------- ONE PAGE CHECKOUT--------------- */

/*
if (location.pathname == "/one-page-checkout.asp") {
    document.writeln('n<link href="v/vspfiles/templates/00_whiteLt/css/one-page-checkout.css" rel="stylesheet" type="text/css" />\n');

    $(document).ready(function() {
        $('#FormatListofErrorsDiv').insertBefore($('#v65-onepage-ContentTable'));
//$('#FormatListofErrorsDiv br').remove();
//$('#v65-onepage-ContentTable br').remove();
        $('table#table_checkout_cart0').attr('width','50%');
        $('<div class="clear"></div>').insertAfter($('#table_checkout_cart0'));
//$('img[src$="v/vspfiles/templates/00_whiteLt/images/clear1x1.gif"][width="5"][height="25"]').hide();
        $('#span_paymentfields_credit_card table').attr('align','left');
        $('#v65-onepage-Detail div').attr('align','left');
    });
}
*/

/* --------------- ACCOUNT SETTINGS--------------- */

/*
if (location.pathname == "/AccountSettings.asp") {
    document.writeln("n<style type='text/css'>#content_area br{display:none;} form#AccountSettingsForm input{background: none repeat scroll 0 0 #FFFFFF !important; border: 1px solid #c8c7c7 !important; color:#231F20 !important; font-size: 11px !important; font-family: 'Lato', Helvetica, Arial, sans-serif; font-style:normal; font-weight: 400; outline: medium none; padding: 2px 6px 1px !important; margin:0px 0px 10px 5px;} form#AccountSettingsForm select{margin:0px 0px 10px 5px;} form#AccountSettingsForm input#btnContinue, form#AccountSettingsForm input[type=image]{margin:0px !important; border:0px !important; padding:0px !important;} form#AccountSettingsForm br{display:none;} form#AccountSettingsForm{padding-bottom:25px;} .rollit{display:none !important;} form#AccountSettingsForm input[type=checkbox], form#AccountSettingsForm input[type=radio]{border:0px !important;} #AccountSettingsForm table{padding-bottom:10px;} .formCont{padding:0px; background-color:#ffffff;} form#AccountSettingsForm input.emailcheck{float:right; margin-bottom:0px !important; margin-top:10px !important;}</style>\n");

    $(document).ready(function() {
        $('table[width="98%"]').attr('width','100%');
        //$('td[width="31%"]').attr('width','100%');
        $('#btnContinue').parent().attr('align','left');
        $('#AccountSettingsForm table td[width="48%"]').attr('width','5%');
        $('#AccountSettingsForm table td[align="right"]').attr('align','left');
        $('#AccountSettingsForm table[width="80%"]').addClass('formCont');
        //$('table.formCont td:nth-child(1)').css('padding-left','325px');
        $('table.formCont').attr('width','100%');
        $('input#btnContinue').parent().parent().addClass('btnremove');
        $('tr.btnremove td:nth-child(1)').append($('input#btnContinue'));

        $('.PageText_L694n p').prepend($('input[name="emailsubscriber"]'));
        $('input[name="emailsubscriber"][type="checkbox"]').addClass('emailcheck');
        $('input[type="checkbox"]').css('margin-right','10px');
        $('input[type="radio"]').css('margin-right','10px');
        $('img[src$="a/a/i/Account/CreditCard_PayOption.gif"]').css('display','none');
        $('img[src$="a/a/i/Account/CreditCard_PayOption.gif"]').parent().parent().attr('width','1%');
        $('td[width="86%"]').attr('width','99%');

    });
}
*/

/* --------------- WISHLIST--------------- */

/*
if (location.pathname == "/WishList.asp") {
    document.writeln("n<style type='text/css'>#content_area br{display:none;} p{padding-right:100px;} .pricecolor{padding-right:5px !important;} .colors_productname{padding-bottom:5px !important;}</style>\n");

    $(document).ready(function() {
        $('#content_area table[width="96%"]').attr('width','100%');
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_addtowishlist_remove.gif"]').parent().addClass('wishclear otherlinks');
        $('<span>Remove from wishlist.</span>').insertBefore($('img[src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_addtowishlist_remove.gif"]'));
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_addtowishlist_remove.gif"]').remove();
        $('.v65-productDisplay td[background="v/vspfiles/templates/00_whiteLt/images/Grid_Single_Divider_Horiz.gif"]').css({'padding':'15px 0px', 'background-position':'0px 15px', 'background-image':'url(v/vspfiles/templates/00_whiteLt/img/template/hline_dash.png)', 'background-repeat':'repeat-x'});
        $('.v65-productDisplay td[background="v/vspfiles/templates/00_whiteLt/images/Grid_Single_Divider_Vert.gif"]').css({'padding':'0px 15px', 'background-position':'15px 6px'});
        $('img[src$="v/vspfiles/templates/00_whiteLt/images/buttons/btn_addtocart_small.gif"]').css('padding-bottom','15px');
    });
}
*/


/* --------------- ORDER FINISHED--------------- */
/*
if (location.pathname == "/OrderFinished.asp") {
    document.writeln("n<style type='text/css'>#content_area br{display:none;} p{padding-right:100px;} </style>\n");

    $(document).ready(function() {
        $('img[width="100"][height="100"]').parent().parent().addClass('iconarea');
        $('img[width="100"][height="100"]').attr({width:'60', height:'60'});
        $('.iconarea td').css('padding-bottom','20px');
        $('#content_area td').attr('align','left');
    });

}
*/